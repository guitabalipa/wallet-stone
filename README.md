## Wallet Stone
A cryptocurrency virtual wallet created for [Stone code challenge].

Features:
- Show list of cryptocurrencies (currently only Bitcoin and Brita) with price
- Show personal balance in each cryptocurrency
- Option to buy each crypto
- Option to sell each crypto
- Option to trade one crypto for another
- Show list of all personal transactions with details
- Register/Login/Logout

### Setup
**Requirements**
- JDK 8
- Latest Android SDK tools
- Latest Android platform tools
- Android SDK 29
- AndroidX

**Dependencies**
- [Mercado Bitcoin API]
- [Banco Central API]
- [Android Jetpack]: material / viewModel / navigation / livedata / room / etc.
- [Retrofit] / [OkHttp] / [Moshi]
- [Dagger2]
- [Timber]
- [ThreeTenABP]

**Build**

    ./gradlew assembleDebug

Run tests:

    ./gradlew testDebug

**Coverage**

    ./gradlew jacocoTestReport


[Stone code challenge]: https://github.com/stone-payments/desafio-mobile/tree/master/wallet
[Mercado Bitcoin API]: https://www.mercadobitcoin.com.br/api-doc/
[Banco Central API]: https://dadosabertos.bcb.gov.br/dataset/taxas-de-cambio-todos-os-boletins-diarios
[Android Jetpack]: https://developer.android.com/jetpack
[Retrofit]: https://github.com/square/retrofit
[OkHttp]: https://github.com/square/okhttp
[Moshi]: https://github.com/square/moshi
[Dagger2]: https://github.com/google/dagger
[Timber]: https://github.com/JakeWharton/timber
[ThreetenABP]: https://github.com/JakeWharton/ThreeTenABP
