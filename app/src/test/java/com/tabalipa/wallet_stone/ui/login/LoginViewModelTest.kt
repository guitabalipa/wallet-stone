package com.tabalipa.wallet_stone.ui.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.tabalipa.wallet_stone.interactor.SaveUserInteractor
import com.tabalipa.wallet_stone.interactor.VerifyLoginInteractor
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class LoginViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    private lateinit var verifyLoginInteractor: VerifyLoginInteractor

    @MockK
    private lateinit var saveUserInteractor: SaveUserInteractor

    private lateinit var loginViewModel: LoginViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        loginViewModel = LoginViewModel(
            saveUserInteractor,
            verifyLoginInteractor
        )
    }

    @Test
    fun `should change feature to register`() {
        loginViewModel.changeFeature()

        assert(loginViewModel.feature.value != null)
        assert(loginViewModel.feature.value == Feature.REGISTER)
    }
}
