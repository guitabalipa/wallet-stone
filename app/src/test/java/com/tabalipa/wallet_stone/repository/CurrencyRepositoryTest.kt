package com.tabalipa.wallet_stone.repository

import com.tabalipa.wallet_stone.datasource.CurrencyDataSource
import com.tabalipa.wallet_stone.datasource.db.dao.CurrencyDao
import com.tabalipa.wallet_stone.model.entity.CurrencyEntity
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

class CurrencyRepositoryTest {

    private lateinit var currencyRepository: CurrencyRepository

    @MockK
    private lateinit var currencyDataSource: CurrencyDataSource

    @MockK
    private lateinit var currencyDao: CurrencyDao

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        currencyRepository = CurrencyRepository(currencyDataSource, currencyDao)
    }

    @Test
    fun `should return item from database`() = runBlocking {
        val currency = CurrencyEntity(name = "Test", price = 3.0)
        coEvery { currencyDao.getAll() } returns listOf(currency)

        val list = currencyRepository.getAllCurrencies("date", "date")

        assertNotNull(list)
        assertEquals(currency.name, list[0].name)
        assertEquals(currency.price, list[0].price, 0.0)
    }
}
