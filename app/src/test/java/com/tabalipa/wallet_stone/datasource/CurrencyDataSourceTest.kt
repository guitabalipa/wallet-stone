package com.tabalipa.wallet_stone.datasource

import com.tabalipa.wallet_stone.api.RestApi
import com.tabalipa.wallet_stone.model.remote.BitcoinInfo
import com.tabalipa.wallet_stone.model.remote.BitcoinResponse
import com.tabalipa.wallet_stone.model.remote.BritaInfo
import com.tabalipa.wallet_stone.model.remote.BritaResponse
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

class CurrencyDataSourceTest {

    @MockK
    private lateinit var restApi: RestApi

    private lateinit var currencyDataSource: CurrencyDataSource

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        currencyDataSource = CurrencyDataSource(restApi)
    }

    @Test
    fun `should return list of remote items`() = runBlocking {
        val btcResponse = getFakeBitcoinResponse()
        val brtResponse = getFakeBritaResponse()
        val initialDate = "date"
        val endDate = "date"

        coEvery { restApi.getBitcoinInfo() } returns btcResponse
        coEvery { restApi.getBritaInfo(initialDate, endDate) } returns brtResponse

        val list = currencyDataSource.getAllCurrencies(initialDate, endDate)

        assertNotNull(list)
        assertEquals(btcResponse.ticker.last.toDouble(), list[0].price, 0.0)
        assertEquals(brtResponse.value[0].cotacaoCompra, list[1].price, 0.0)
    }

    private fun getFakeBitcoinResponse(): BitcoinResponse {
        return BitcoinResponse(
            BitcoinInfo(
                "0", "0", "0", "1", "0", "0", "0"
            )
        )
    }

    private fun getFakeBritaResponse(): BritaResponse {
        return BritaResponse(
            "0",
            listOf(
                BritaInfo(
                    1.0, 0.0, "0"
                )
            )
        )
    }
}
