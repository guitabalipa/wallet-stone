package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.junit.Before

import org.junit.Test

class LogUserInteractorTest {

    @MockK
    private lateinit var userInfoLocalDataSource: UserInfoLocalDataSource

    private lateinit var logUserInteractor: LogUserInteractor

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        logUserInteractor = LogUserInteractor(userInfoLocalDataSource)
    }

    @Test
    fun `should call data source`() = runBlocking {
        coEvery { userInfoLocalDataSource.logUser() } returns Unit
        logUserInteractor.logUser()
        verify(exactly = 1) { userInfoLocalDataSource.logUser() }
    }
}
