package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.db.dao.UserDao
import com.tabalipa.wallet_stone.model.entity.UserEntity
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

class GetUserInteractorTest {

    @MockK
    private lateinit var userDao: UserDao

    private lateinit var getUserInteractor: GetUserInteractor

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        getUserInteractor = GetUserInteractor(userDao)
    }

    @Test
    fun `should return user from user dao`() = runBlocking {
        val username = "test"
        val userEntity = UserEntity(username, "123")
        coEvery { userDao.getUser(username) } returns userEntity
        val user = getUserInteractor.getUser(username)
        assertNotNull(user)
        assertEquals(user.username, userEntity.username)
        assertEquals(user.password, userEntity.password)
    }
}
