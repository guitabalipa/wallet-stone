package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.db.dao.UserDao
import com.tabalipa.wallet_stone.model.domain.Balance
import com.tabalipa.wallet_stone.model.domain.User
import com.tabalipa.wallet_stone.model.entity.UserEntity
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class SaveUserInteractorTest {

    @MockK
    private lateinit var userDao: UserDao

    @MockK
    private lateinit var setUsernameInteractor: SetUsernameInteractor

    @MockK
    private lateinit var saveBalanceInteractor: SaveBalanceInteractor

    @MockK
    private lateinit var logUserInteractor: LogUserInteractor

    private lateinit var saveUserInteractor: SaveUserInteractor

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        saveUserInteractor = SaveUserInteractor(
            userDao,
            setUsernameInteractor,
            saveBalanceInteractor,
            logUserInteractor
        )
    }

    @Test
    fun `should return success if user info is saved into local database`() = runBlocking {
        val user = User("test", "123")
        val userEntity = UserEntity("test", "123")
        val balance = Balance(user.username, "test", 1.0)
        coEvery { userDao.insert(userEntity) } returns Unit
        coEvery { setUsernameInteractor.setUsername(user.username) } returns Unit
        coEvery { saveBalanceInteractor.save(balance) } returns Unit
        coEvery { logUserInteractor.logUser() } returns Unit
        val result = saveUserInteractor.save(user, balance)
        coVerify(exactly = 1) {
            userDao.insert(userEntity)
            setUsernameInteractor.setUsername(user.username)
            saveBalanceInteractor.save(balance)
            logUserInteractor.logUser()
        }
        assertEquals(result, SaveUserInteractor.Result.Success)
    }

    @Test
    fun `should return failure if exception occur`() = runBlocking {
        val user = User("test", "123")
        val userEntity = UserEntity("test", "123")
        val balance = Balance(user.username, "test", 1.0)
        coEvery { userDao.insert(userEntity) } throws Exception()
        val result = saveUserInteractor.save(user, balance)
        assertEquals(result, SaveUserInteractor.Result.Failure)
    }
}
