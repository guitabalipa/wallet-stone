package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.Before
import org.junit.Test

class LogoutUserInteractorTest {

    private lateinit var logoutUserInteractor: LogoutUserInteractor

    @MockK
    private lateinit var userInfoLocalDataSource: UserInfoLocalDataSource

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        logoutUserInteractor = LogoutUserInteractor(userInfoLocalDataSource)
    }

    @Test
    fun `should logout user`() {
        every { userInfoLocalDataSource.logout() } returns Unit
        userInfoLocalDataSource.logout()
        verify(exactly = 1) { userInfoLocalDataSource.logout() }
    }
}
