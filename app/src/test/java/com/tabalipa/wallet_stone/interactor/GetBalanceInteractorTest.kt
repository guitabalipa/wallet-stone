package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.Constants
import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import com.tabalipa.wallet_stone.datasource.db.dao.BalanceDao
import com.tabalipa.wallet_stone.model.entity.BalanceEntity
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class GetBalanceInteractorTest {

    private lateinit var getBalanceInteractor: GetBalanceInteractor

    @MockK
    private lateinit var balanceDao: BalanceDao

    @MockK
    private lateinit var userInfoLocalDataSource: UserInfoLocalDataSource

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        getBalanceInteractor = GetBalanceInteractor(balanceDao, userInfoLocalDataSource)
    }

    @Test
    fun `should return available balance`() = runBlocking {
        val username = "test"
        val currency = Constants.DEFAULT_CURRENCY
        val balanceEntity = BalanceEntity(username, currency, 1.0)
        coEvery { userInfoLocalDataSource.getUsername() } returns username
        coEvery { balanceDao.getBalanceByCurrency(username, currency) } returns balanceEntity
        val amount = getBalanceInteractor.getBalanceAvailableAmount()
        assertEquals(amount, balanceEntity.amount, .0)
    }
}
