package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.db.dao.UserDao
import com.tabalipa.wallet_stone.model.domain.User
import com.tabalipa.wallet_stone.model.entity.UserEntity
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class VerifyLoginInteractorTest {

    private lateinit var verifyLoginInteractor: VerifyLoginInteractor

    @MockK
    private lateinit var userDao: UserDao

    @MockK
    private lateinit var logUserInteractor: LogUserInteractor

    @MockK
    private lateinit var setUsernameInteractor: SetUsernameInteractor

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        verifyLoginInteractor = VerifyLoginInteractor(
            userDao,
            logUserInteractor,
            setUsernameInteractor
        )
    }

    @Test
    fun `should login user with success`() = runBlocking {
        val user = User("test", "test")
        val userEntity = UserEntity("test", "test")
        coEvery { userDao.getUser(user.username) } returns userEntity
        coEvery { logUserInteractor.logUser() } returns Unit
        coEvery { setUsernameInteractor.setUsername(user.username) } returns Unit
        val result = verifyLoginInteractor.verify(user)
        assertEquals(result, VerifyLoginInteractor.Result.Success)
    }

    @Test
    fun `should failed to login user`() = runBlocking {
        val user = User("test", "test")
        val userEntity = UserEntity("test", "test-failed")
        coEvery { userDao.getUser(user.username) } returns userEntity
        coEvery { logUserInteractor.logUser() } returns Unit
        val result = verifyLoginInteractor.verify(user)
        assertEquals(result, VerifyLoginInteractor.Result.Failure)
    }
}
