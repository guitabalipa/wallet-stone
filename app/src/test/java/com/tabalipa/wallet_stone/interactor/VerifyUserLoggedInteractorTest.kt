package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test

class VerifyUserLoggedInteractorTest {

    private lateinit var verifyUserLoggedInteractor: VerifyUserLoggedInteractor

    @MockK
    private lateinit var userInfoLocalDataSource: UserInfoLocalDataSource

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        verifyUserLoggedInteractor = VerifyUserLoggedInteractor(userInfoLocalDataSource)
    }

    @Test
    fun `should return true if user is logged`() = runBlocking {
        val booleanResult = true
        every { userInfoLocalDataSource.isUserLogged() } returns booleanResult
        val result = verifyUserLoggedInteractor.isUserLogged()
        assertEquals(result, booleanResult)
    }
}
