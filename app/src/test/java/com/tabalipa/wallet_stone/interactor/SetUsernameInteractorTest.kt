package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class SetUsernameInteractorTest {

    @MockK
    private lateinit var userInfoLocalDataSource: UserInfoLocalDataSource

    private lateinit var setUsernameInteractor: SetUsernameInteractor

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        setUsernameInteractor = SetUsernameInteractor(userInfoLocalDataSource)
    }

    @Test
    fun `should pass username to data source`() = runBlocking {
        val name = "test"
        coEvery { userInfoLocalDataSource.setUsername(name) } returns Unit
        setUsernameInteractor.setUsername(name)
        verify(exactly = 1) { userInfoLocalDataSource.setUsername(name) }
    }
}
