package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import com.tabalipa.wallet_stone.datasource.db.dao.BalanceDao
import com.tabalipa.wallet_stone.helper.BalanceHelper
import com.tabalipa.wallet_stone.model.entity.BalanceEntity
import com.tabalipa.wallet_stone.model.entity.CurrencyEntity
import com.tabalipa.wallet_stone.repository.CurrencyRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

class GetAllCurrenciesInteractorTest {

    private lateinit var getAllCurrenciesInteractor: GetAllCurrenciesInteractor

    @MockK
    private lateinit var repository: CurrencyRepository

    @MockK
    private lateinit var balanceDao: BalanceDao

    @MockK
    private lateinit var balanceHelper: BalanceHelper

    @MockK
    private lateinit var infoLocalDataSource: UserInfoLocalDataSource

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        getAllCurrenciesInteractor = GetAllCurrenciesInteractor(
            repository,
            balanceDao,
            balanceHelper,
            infoLocalDataSource
        )
    }

    @Test
    fun `should return list of currency items`() = runBlocking {
        val item = CurrencyEntity("test", 1.0)
        val balance = BalanceEntity("test", "test", .0)
        val initialDate = "date"
        val endDate = "date"
        val balanceResult = .0
        val username = "test"

        coEvery { repository.getAllCurrencies(initialDate, endDate) } returns listOf(item)
        coEvery { balanceDao.getAll() } returns listOf(balance)
        coEvery { balanceHelper.calculateBalance(item.price, balance.amount) } returns balanceResult
        coEvery { infoLocalDataSource.getUsername() } returns username

        val items = getAllCurrenciesInteractor.getAllCurrencies(initialDate, endDate)
        assertNotNull(items)
        assertEquals(items[0].name, item.name)
        assertEquals(items[0].price, item.price, .0)
        assertEquals(items[0].balanceUnit, balance.amount, .0)
        assertEquals(items[0].balance, balanceResult, .0)
    }
}
