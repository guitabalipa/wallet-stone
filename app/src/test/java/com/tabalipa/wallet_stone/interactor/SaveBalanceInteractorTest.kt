package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.db.dao.BalanceDao
import com.tabalipa.wallet_stone.model.domain.Balance
import com.tabalipa.wallet_stone.model.entity.BalanceEntity
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class SaveBalanceInteractorTest {

    @MockK
    private lateinit var balanceDao: BalanceDao

    private lateinit var saveBalanceInteractor: SaveBalanceInteractor

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        saveBalanceInteractor = SaveBalanceInteractor(balanceDao)
    }

    @Test
    fun `should pass balance to balance dao`() = runBlocking {
        val balance = Balance("test", "test", 1.0)
        val balanceEntity = BalanceEntity("test", "test", 1.0)
        coEvery { balanceDao.insert(balanceEntity) } returns Unit
        saveBalanceInteractor.save(balance)
        coVerify(exactly = 1) { balanceDao.insert(balanceEntity) }
    }
}
