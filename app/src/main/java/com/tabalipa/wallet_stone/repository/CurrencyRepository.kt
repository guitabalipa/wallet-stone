package com.tabalipa.wallet_stone.repository

import com.tabalipa.wallet_stone.datasource.CurrencyDataSource
import com.tabalipa.wallet_stone.datasource.db.dao.CurrencyDao
import com.tabalipa.wallet_stone.model.entity.CurrencyEntity
import com.tabalipa.wallet_stone.model.mapper.toEntity
import timber.log.Timber
import javax.inject.Inject

class CurrencyRepository @Inject constructor(
    private val currencyDataSource: CurrencyDataSource,
    private val currencyDao: CurrencyDao
) {

    suspend fun getAllCurrencies(initialDate: String, endDate: String): List<CurrencyEntity> {
        try {
            val currencies = currencyDataSource.getAllCurrencies(initialDate, endDate).map { it.toEntity() }
            currencyDao.insertAll(currencies)
        } catch (e: Exception) {
            Timber.e(e)
        }
        return currencyDao.getAll()
    }

    suspend fun getCurrency(currencyId: String): CurrencyEntity {
        return currencyDao.getCurrencyById(currencyId)
    }
}
