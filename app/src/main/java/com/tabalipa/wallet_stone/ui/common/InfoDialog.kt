package com.tabalipa.wallet_stone.ui.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tabalipa.wallet_stone.databinding.DialogInfoBinding

class InfoDialog : BottomSheetDialogFragment() {

    private lateinit var binding: DialogInfoBinding

    companion object {
        private const val ARG_DRAWABLE = "arg_drawable"
        private const val ARG_TEXT = "arg_text"
        fun show(
            manager: FragmentManager,
            @DrawableRes infoIcon: Int,
            @StringRes infoText: Int
        ) = InfoDialog().apply {
            arguments = Bundle().apply {
                putInt(ARG_DRAWABLE, infoIcon)
                putInt(ARG_TEXT, infoText)
            }
            show(manager, "info-dialog")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogInfoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        val icon = requireArguments().getInt(ARG_DRAWABLE)
        val text = requireArguments().getInt(ARG_TEXT)
        with(binding) {
            imageView.setImageDrawable(ContextCompat.getDrawable(requireContext(), icon))
            textView.text = getString(text)
        }
    }
}
