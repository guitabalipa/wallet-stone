package com.tabalipa.wallet_stone.ui.splash

import android.os.Bundle
import androidx.activity.viewModels
import com.tabalipa.wallet_stone.ui.base.BaseActivity
import com.tabalipa.wallet_stone.ui.login.LoginActivity
import com.tabalipa.wallet_stone.ui.main.MainActivity

class SplashActivity : BaseActivity() {

    private val viewModel: SplashViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)
        super.onCreate(savedInstanceState)
        setupObservables()
    }

    private fun setupObservables() {
        viewModel.isUserLogged.observe(this, { isUserLogged ->
            if (isUserLogged) {
                navigateToMainScreen()
            } else {
                navigateToLoginScreen()
            }
        })
    }

    private fun navigateToMainScreen() {
        startActivity(MainActivity.newIntent(this))
    }

    private fun navigateToLoginScreen() {
        startActivity(LoginActivity.newIntent(this))
    }
}
