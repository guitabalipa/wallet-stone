package com.tabalipa.wallet_stone.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tabalipa.wallet_stone.Constants
import com.tabalipa.wallet_stone.interactor.SaveUserInteractor
import com.tabalipa.wallet_stone.interactor.VerifyLoginInteractor
import com.tabalipa.wallet_stone.model.domain.Balance
import com.tabalipa.wallet_stone.model.domain.User
import com.tabalipa.wallet_stone.ui.base.SingleLiveEvent
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val saveUserInteractor: SaveUserInteractor,
    private val verifyLoginInteractor: VerifyLoginInteractor
) : ViewModel() {

    private val _feature = MutableLiveData<Feature>().apply { value = Feature.LOGIN }
    val feature: LiveData<Feature> = _feature

    private val _loading = MutableLiveData<Boolean>().apply { value = false }
    val loading: LiveData<Boolean> = _loading

    val navigateToMainScreen = SingleLiveEvent<Boolean>()
    val showError = SingleLiveEvent<Boolean>()
    val showPasswordError = SingleLiveEvent<Boolean>()

    fun changeFeature() {
        _feature.value = when (feature.value) {
            Feature.LOGIN -> Feature.REGISTER
            else -> Feature.LOGIN
        }
    }

    fun login(user: User) {
        _loading.value = true
        when (feature.value) {
            Feature.REGISTER -> saveUser(user)
            else -> logUser(user)
        }
    }

    private fun saveUser(user: User) {
        viewModelScope.launch {
            val result = saveUserInteractor.save(
                user,
                Balance(user.username, Constants.DEFAULT_CURRENCY, 100000.00)
            )

            if (result is SaveUserInteractor.Result.Success) {
                navigateToMainScreen.value = true
            } else {
                showError.value = true
            }

            _loading.value = false
        }
    }

    private fun logUser(user: User) {
        viewModelScope.launch {
            val result = verifyLoginInteractor.verify(user)

            if (result is VerifyLoginInteractor.Result.Success) {
                navigateToMainScreen.value = true
            } else {
                showPasswordError.value = true
            }

            _loading.value = false
        }
    }
}

enum class Feature {
    LOGIN,
    REGISTER
}
