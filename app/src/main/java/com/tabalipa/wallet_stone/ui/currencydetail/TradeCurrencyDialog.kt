package com.tabalipa.wallet_stone.ui.currencydetail

import android.content.Context
import android.os.Bundle
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.FragmentManager
import com.tabalipa.wallet_stone.R
import com.tabalipa.wallet_stone.databinding.DialogTradeBinding
import com.tabalipa.wallet_stone.helper.BalanceHelper
import com.tabalipa.wallet_stone.model.domain.CurrencyItem
import com.tabalipa.wallet_stone.model.domain.TradeItem
import com.tabalipa.wallet_stone.ui.base.BaseDialogFragment
import com.tabalipa.wallet_stone.ui.utils.FormatUtils
import javax.inject.Inject

class TradeCurrencyDialog : BaseDialogFragment() {

    @Inject
    lateinit var helper: BalanceHelper

    private lateinit var binding: DialogTradeBinding
    private var listener: TradeListener? = null
    private var valueTextWatcher: TextWatcher? = null
    private var unit: Double = .0
    private var purchaseAmount: Double = .0

    private lateinit var selectedCurrency: CurrencyItem
    private lateinit var tradeItem: TradeItem

    companion object {
        private const val ARG_TRADE_ITEM = "arg_trade_item"
        fun show(
            manager: FragmentManager,
            tradeItem: TradeItem
        ) = TradeCurrencyDialog().apply {
            arguments = Bundle().apply {
                putParcelable(ARG_TRADE_ITEM, tradeItem)
            }
            show(manager, "purchase-dialog")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (parentFragment is TradeListener) {
            listener = parentFragment as TradeListener
        } else {
            throw IllegalArgumentException("Parent fragment must implement ${TradeListener::class.qualifiedName}")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogTradeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tradeItem = requireArguments().getParcelable(ARG_TRADE_ITEM)!!
        setupSpinner()
        setupViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        binding.editTextValue.removeTextChangedListener(valueTextWatcher)
        valueTextWatcher = null
    }

    private fun setupViews() {
        with(binding) {
            textViewBalance.text =
                getString(R.string.balance_available, FormatUtils.formatCurrency(tradeItem.balance))

            buttonUseAllBalance.setOnClickListener {
                editTextValue.setText(FormatUtils.formatCurrency(tradeItem.balance))
            }

            buttonPurchase.setOnClickListener {
                if (isValid()) {
                    listener?.onTradeClicked(selectedCurrency, helper.getUnitAmount(purchaseAmount, tradeItem.price), unit)
                    dismiss()
                }
            }

            valueTextWatcher = FormatUtils.createCurrencyValueChangedListener(editTextValue) {
                purchaseAmount = it
                unit = helper.getUnitAmount(it, selectedCurrency.price)
                textViewUnit.text = getString(
                    R.string.trade_unit_label,
                    FormatUtils.formatUnit(unit),
                    selectedCurrency.name
                )
            }
            editTextValue.addTextChangedListener(valueTextWatcher)
            editTextValue.doAfterTextChanged { textInputLayoutValue.error = "" }
            editTextValue.setText("0")
        }
    }

    private fun setupSpinner() {
        with(binding.currencySpinner) {
            val currenciesName = tradeItem.currencies.map { it.name }
            selectedCurrency = tradeItem.currencies.first()
            adapter = ArrayAdapter(
                requireContext(),
                android.R.layout.simple_spinner_dropdown_item,
                currenciesName
            )
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val label = parent!!.getItemAtPosition(position) as String
                    selectedCurrency = tradeItem.currencies.find { it.name == label }!!
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
        }
    }

    private fun isValid(): Boolean {
        with(binding) {
            if (editTextValue.text.isNullOrBlank()) {
                textInputLayoutValue.error = getString(R.string.mandatory_field)
                return false
            }

            if (purchaseAmount < 1) {
                textInputLayoutValue.error = getString(R.string.trade_minimum)
                return false
            }

            if (purchaseAmount > FormatUtils.formatDoubleValue(tradeItem.balance)) {
                textInputLayoutValue.error = getString(R.string.balance_insufficient)
                return false
            }

            return true
        }
    }

    interface TradeListener {
        fun onTradeClicked(item: CurrencyItem, unitTrade: Double, unitTraded: Double)
    }
}
