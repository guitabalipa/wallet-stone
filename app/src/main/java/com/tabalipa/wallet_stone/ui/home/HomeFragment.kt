package com.tabalipa.wallet_stone.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.tabalipa.wallet_stone.databinding.FragmentHomeBinding
import com.tabalipa.wallet_stone.model.domain.CurrencyItem
import com.tabalipa.wallet_stone.ui.base.BaseFragment
import com.tabalipa.wallet_stone.ui.utils.FormatUtils

class HomeFragment : BaseFragment() {

    private val viewModel: HomeViewModel by viewModels { viewModelFactory }

    private lateinit var binding: FragmentHomeBinding
    private lateinit var adapter: CurrencyAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setupObservables()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBalance()
        viewModel.getCurrencies()
    }

    private fun setupViews() {
        adapter = CurrencyAdapter(::navigateToDetailFragment)

        with(binding) {
            recycler.isNestedScrollingEnabled = false
            recycler.layoutManager = LinearLayoutManager(requireContext())
            recycler.adapter = adapter

            swipeRefreshLayout.setOnRefreshListener {
                viewModel.getBalance()
                viewModel.getCurrencies()
            }
        }
    }

    private fun setupObservables() {
        viewModel.items.observe(viewLifecycleOwner, {
            adapter.submitList(it)
        })

        viewModel.loading.observe(viewLifecycleOwner, ::renderLoading)
        viewModel.balance.observe(viewLifecycleOwner, ::renderBalance)
    }

    private fun renderLoading(isLoading: Boolean) {
        binding.swipeRefreshLayout.isRefreshing = isLoading
    }

    private fun renderBalance(balance: Double) {
        binding.textViewBalanceValue.text = FormatUtils.formatCurrency(balance)
    }

    private fun navigateToDetailFragment(item: CurrencyItem) {
        findNavController().navigate(
            HomeFragmentDirections.actionFragmentHomeToFragmentCurrencyDetail(item.name)
        )
    }
}
