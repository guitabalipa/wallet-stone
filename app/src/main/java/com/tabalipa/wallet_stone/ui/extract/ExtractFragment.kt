package com.tabalipa.wallet_stone.ui.extract

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.tabalipa.wallet_stone.databinding.FragmentExtractBinding
import com.tabalipa.wallet_stone.helper.BalanceHelper
import com.tabalipa.wallet_stone.ui.base.BaseFragment
import javax.inject.Inject

class ExtractFragment : BaseFragment() {

    @Inject lateinit var helper: BalanceHelper

    private val viewModel: ExtractViewModel by viewModels { viewModelFactory }

    private lateinit var binding: FragmentExtractBinding
    private lateinit var adapter: ExtractAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentExtractBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setupObservables()
    }

    private fun setupViews() {
        adapter = ExtractAdapter(helper)

        with(binding) {
            recycler.layoutManager = LinearLayoutManager(requireContext())
            recycler.adapter = adapter

            swipeRefreshLayout.setOnRefreshListener {
                viewModel.loadExtract()
            }
        }
    }

    private fun setupObservables() {
        viewModel.loading.observe(viewLifecycleOwner, ::renderLoading)
        viewModel.items.observe(viewLifecycleOwner, { items ->
            binding.recycler.isVisible = items.isNotEmpty()
            binding.textViewEmpty.isVisible = items.isEmpty()
            adapter.submitList(items)
        })
    }

    private fun renderLoading(isLoading: Boolean) {
        binding.swipeRefreshLayout.isRefreshing = isLoading
    }
}
