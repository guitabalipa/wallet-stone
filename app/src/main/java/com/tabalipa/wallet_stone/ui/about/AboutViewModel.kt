package com.tabalipa.wallet_stone.ui.about

import androidx.lifecycle.ViewModel
import com.tabalipa.wallet_stone.interactor.LogoutUserInteractor
import com.tabalipa.wallet_stone.ui.base.SingleLiveEvent
import javax.inject.Inject

class AboutViewModel @Inject constructor(
    private val logoutUserInteractor: LogoutUserInteractor
) : ViewModel() {

    val navigateToLoginScreen = SingleLiveEvent<Boolean>()

    fun logout() {
        logoutUserInteractor.logout()
        navigateToLoginScreen.value = true
    }
}
