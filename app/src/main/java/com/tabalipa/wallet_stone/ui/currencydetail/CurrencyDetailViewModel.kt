package com.tabalipa.wallet_stone.ui.currencydetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tabalipa.wallet_stone.interactor.*
import com.tabalipa.wallet_stone.model.domain.CurrencyItem
import com.tabalipa.wallet_stone.model.domain.TradeItem
import com.tabalipa.wallet_stone.model.domain.TradeTransaction
import com.tabalipa.wallet_stone.model.domain.Transaction
import com.tabalipa.wallet_stone.ui.base.SingleLiveEvent
import com.tabalipa.wallet_stone.ui.utils.getCurrentTimeInMillis
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class CurrencyDetailViewModel @Inject constructor(
    private val getBalanceInteractor: GetBalanceInteractor,
    private val savePurchaseTransactionInteractor: SavePurchaseTransactionInteractor,
    private val saveSellTransactionInteractor: SaveSellTransactionInteractor,
    private val getCurrencyInteractor: GetCurrencyInteractor,
    private val getTradeCurrenciesInteractor: GetTradeCurrenciesInteractor,
    private val saveTradeTransactionInteractor: SaveTradeTransactionInteractor
) : ViewModel() {

    private val _balance = MutableLiveData<Double>()
    val balance: LiveData<Double> = _balance

    private val _currency = MutableLiveData<CurrencyItem>()
    val currency: LiveData<CurrencyItem> = _currency

    val showPurchaseScreen = SingleLiveEvent<Pair<Double, Double>>()
    val showSellCurrencyScreen = SingleLiveEvent<Pair<Double, Double>>()
    val showTradeCurrencyScreen = SingleLiveEvent<TradeItem>()
    val showSuccessScreen = SingleLiveEvent<Int>()
    val showFailedScreen = SingleLiveEvent<Boolean>()

    init {
        getBalance()
    }

    fun getBalance() {
        viewModelScope.launch {
            _balance.value = getBalanceInteractor.getBalanceAvailableAmount()
        }
    }

    fun getCurrency(currencyId: String) {
        viewModelScope.launch {
            try {
                _currency.value = getCurrencyInteractor.getCurrency(currencyId)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun purchase(purchased: Double) {
        viewModelScope.launch {
            val currency = currency.value!!
            val result = savePurchaseTransactionInteractor.save(
                Transaction(
                    currency.name,
                    TransactionType.PURCHASE.name,
                    getCurrentTimeInMillis(),
                    currency.price,
                    convertUnit(purchased, currency.price),
                    purchased
                ),
                currency.balanceUnit,
                balance.value!!
            )

            when (result) {
                is SavePurchaseTransactionInteractor.Result.Success -> showSuccessScreen.value = result.text
                is SavePurchaseTransactionInteractor.Result.Failure -> showFailedScreen.value = true
            }
        }
    }

    fun sell(sold: Double) {
        viewModelScope.launch {
            val currency = currency.value!!
            val result = saveSellTransactionInteractor.save(
                Transaction(
                    currency.name,
                    TransactionType.SELL.name,
                    getCurrentTimeInMillis(),
                    currency.price,
                    convertUnit(sold, currency.price),
                    sold
                ),
                currency.balanceUnit,
                balance.value!!
            )

            when (result) {
                is SaveSellTransactionInteractor.Result.Success -> showSuccessScreen.value = result.text
                is SaveSellTransactionInteractor.Result.Failure -> showFailedScreen.value = true
            }
        }
    }
    
    fun trade(item: CurrencyItem, unitTrade: Double, unitTraded: Double) {
        viewModelScope.launch {
            val currency = currency.value!!
            val result = saveTradeTransactionInteractor.save(
                TradeTransaction(
                    currency.name,
                    item.name,
                    TransactionType.TRADE.name,
                    getCurrentTimeInMillis(),
                    currency.price,
                    item.price,
                    unitTrade,
                    unitTraded
                ),
                currency.balanceUnit
            )

            when (result) {
                is SaveTradeTransactionInteractor.Result.Success -> showSuccessScreen.value = result.text
                is SaveTradeTransactionInteractor.Result.Failure -> showFailedScreen.value = true
            }
        }
    }

    private fun convertUnit(unit: Double, price: Double): Double {
        return unit * price
    }

    fun showPurchaseScreen() {
        showPurchaseScreen.value = Pair(balance.value!!, currency.value!!.price)
    }

    fun showSellCurrencyScreen() {
        val currency = currency.value!!
        showSellCurrencyScreen.value = Pair(currency.balance, currency.price)
    }

    fun showTradeCurrencyScreen() {
        viewModelScope.launch {
            val currency = currency.value!!
            val tradeCurrencies = getTradeCurrenciesInteractor.getCurrencies(currency.name)
            showTradeCurrencyScreen.value = TradeItem(currency.balance, currency.price, tradeCurrencies)
        }
    }
}

enum class TransactionType {
    PURCHASE,
    SELL,
    TRADE
}
