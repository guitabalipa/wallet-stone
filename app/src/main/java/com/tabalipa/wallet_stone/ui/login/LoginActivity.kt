package com.tabalipa.wallet_stone.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import com.tabalipa.wallet_stone.R
import com.tabalipa.wallet_stone.databinding.ActivityLoginBinding
import com.tabalipa.wallet_stone.model.domain.User
import com.tabalipa.wallet_stone.ui.base.BaseActivity
import com.tabalipa.wallet_stone.ui.main.MainActivity

class LoginActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context) = Intent(context, LoginActivity::class.java)
    }

    private val viewModel: LoginViewModel by viewModels { viewModelFactory }

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupViews()
        setupObservables()
    }

    private fun setupViews() {
        with(binding) {
            buttonRegister.setOnClickListener {
                viewModel.changeFeature()
            }

            buttonEnter.setOnClickListener {
                if (isValid()) {
                    viewModel.login(
                        User(
                            username = editTextUsername.text.toString(),
                            password = editTextPassword.text.toString()
                        )
                    )
                }
            }

            editTextUsername.doAfterTextChanged { textInputLayoutUsername.error = "" }
            editTextPassword.doAfterTextChanged { textInputLayoutPassword.error = "" }
            editTextPasswordConfirm.doAfterTextChanged { textInputLayoutPasswordConfirm.error = "" }
        }
    }

    private fun setupObservables() {
        viewModel.feature.observe(this, ::renderFeature)
        viewModel.loading.observe(this, ::renderLoading)

        viewModel.navigateToMainScreen.observe(this, {
            navigateToMainScreen()
        })

        viewModel.showError.observe(this, {
            binding.textInputLayoutUsername.error = getString(R.string.user_already_registered)
        })

        viewModel.showPasswordError.observe(this, {
            binding.textInputLayoutPassword.error = getString(R.string.invalid_password)
        })
    }

    private fun renderLoading(isLoading: Boolean) {
        binding.loading.isVisible = isLoading
    }

    private fun renderFeature(feature: Feature) {
        when (feature) {
            Feature.REGISTER -> setupRegister()
            else -> setupLogin()
        }
    }

    private fun setupRegister() {
        with(binding) {
            textInputLayoutPasswordConfirm.isVisible = true
            buttonEnter.text = getString(R.string.register)
            buttonRegister.text = getString(R.string.enter)
        }
    }

    private fun setupLogin() {
        with(binding) {
            textInputLayoutPasswordConfirm.isVisible = false
            buttonEnter.text = getString(R.string.enter)
            buttonRegister.text = getString(R.string.register)
        }
    }

    private fun isValid(): Boolean {
        with(binding) {
            if (editTextUsername.text.isNullOrBlank()) {
                textInputLayoutUsername.error = getString(R.string.mandatory_field)
                return false
            }

            if (editTextPassword.text.isNullOrBlank()) {
                textInputLayoutPassword.error = getString(R.string.mandatory_field)
                return false
            }

            if (textInputLayoutPasswordConfirm.isVisible) {
                if (editTextPasswordConfirm.text.isNullOrBlank()) {
                    textInputLayoutPasswordConfirm.error = getString(R.string.mandatory_field)
                    return false
                }

                if (editTextPasswordConfirm.text.toString() != editTextPassword.text.toString()) {
                    textInputLayoutPasswordConfirm.error = getString(R.string.need_same_password)
                    return false
                }
            }
        }

        return true
    }

    private fun navigateToMainScreen() {
        startActivity(MainActivity.newIntent(this))
    }
}
