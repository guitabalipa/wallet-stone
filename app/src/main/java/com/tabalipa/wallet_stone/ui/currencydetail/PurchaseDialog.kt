package com.tabalipa.wallet_stone.ui.currencydetail

import android.content.Context
import android.os.Bundle
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.FragmentManager
import com.tabalipa.wallet_stone.R
import com.tabalipa.wallet_stone.databinding.DialogPurchaseBinding
import com.tabalipa.wallet_stone.helper.BalanceHelper
import com.tabalipa.wallet_stone.ui.base.BaseDialogFragment
import com.tabalipa.wallet_stone.ui.utils.FormatUtils
import javax.inject.Inject
import kotlin.properties.Delegates

class PurchaseDialog : BaseDialogFragment() {

    @Inject
    lateinit var helper: BalanceHelper

    private lateinit var binding: DialogPurchaseBinding
    private var listener: PurchaseListener? = null
    private var valueTextWatcher: TextWatcher? = null
    private var unit: Double = .0
    private var purchaseAmount: Double = .0

    private var balance: Double by Delegates.notNull()
    private var price: Double by Delegates.notNull()

    companion object {
        private const val ARG_BALANCE = "arg_balance"
        private const val ARG_PRICE = "arg_price"
        fun show(
            manager: FragmentManager,
            balance: Double,
            price: Double
        ) = PurchaseDialog().apply {
            arguments = Bundle().apply {
                putDouble(ARG_BALANCE, balance)
                putDouble(ARG_PRICE, price)
            }
            show(manager, "purchase-dialog")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (parentFragment is PurchaseListener) {
            listener = parentFragment as PurchaseListener
        } else {
            throw IllegalArgumentException("Parent fragment must implement ${PurchaseListener::class.qualifiedName}")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogPurchaseBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        balance = requireArguments().getDouble(ARG_BALANCE)
        price = requireArguments().getDouble(ARG_PRICE)
        setupViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        binding.editTextValue.removeTextChangedListener(valueTextWatcher)
        valueTextWatcher = null
    }

    private fun setupViews() {
        with(binding) {
            textViewBalance.text =
                getString(R.string.balance_available, FormatUtils.formatCurrency(balance))

            buttonUseAllBalance.setOnClickListener {
                editTextValue.setText(FormatUtils.formatCurrency(balance))
            }

            buttonPurchase.setOnClickListener {
                if (isValid()) {
                    listener?.onPurchaseClicked(unit)
                    dismiss()
                }
            }

            valueTextWatcher = FormatUtils.createCurrencyValueChangedListener(editTextValue) {
                purchaseAmount = it
                unit = helper.getUnitAmount(it, price)
                textViewUnit.text =
                    getString(R.string.purchase_unit_label, FormatUtils.formatUnit(unit))
            }
            editTextValue.addTextChangedListener(valueTextWatcher)
            editTextValue.doAfterTextChanged { textInputLayoutValue.error = "" }
            editTextValue.setText("0")
        }
    }

    private fun isValid(): Boolean {
        with(binding) {
            if (editTextValue.text.isNullOrBlank()) {
                textInputLayoutValue.error = getString(R.string.mandatory_field)
                return false
            }

            if (purchaseAmount < 1) {
                textInputLayoutValue.error = getString(R.string.purchase_minimum)
                return false
            }

            if (purchaseAmount > FormatUtils.formatDoubleValue(balance)) {
                textInputLayoutValue.error = getString(R.string.balance_insufficient)
                return false
            }

            return true
        }
    }

    interface PurchaseListener {
        fun onPurchaseClicked(unit: Double)
    }
}
