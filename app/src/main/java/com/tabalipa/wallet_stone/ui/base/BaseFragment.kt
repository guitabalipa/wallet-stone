package com.tabalipa.wallet_stone.ui.base

import androidx.fragment.app.Fragment
import com.tabalipa.wallet_stone.ui.viewmodel.ViewModelFactory
import javax.inject.Inject

open class BaseFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val presentationComponent by lazy {
        (requireActivity() as BaseActivity).activityComponent.presentationComponent()
    }

    protected val injector get() = presentationComponent
}
