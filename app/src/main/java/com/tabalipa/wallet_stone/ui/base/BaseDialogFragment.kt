package com.tabalipa.wallet_stone.ui.base

import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tabalipa.wallet_stone.ui.viewmodel.ViewModelFactory
import javax.inject.Inject

open class BaseDialogFragment : BottomSheetDialogFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val presentationComponent by lazy {
        (requireActivity() as BaseActivity).activityComponent.presentationComponent()
    }

    protected val injector get() = presentationComponent
}
