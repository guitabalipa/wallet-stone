package com.tabalipa.wallet_stone.ui.currencydetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.tabalipa.wallet_stone.R
import com.tabalipa.wallet_stone.databinding.FragmentCurrencyDetailBinding
import com.tabalipa.wallet_stone.model.domain.CurrencyItem
import com.tabalipa.wallet_stone.ui.common.InfoDialog
import com.tabalipa.wallet_stone.ui.base.BaseFragment
import com.tabalipa.wallet_stone.ui.utils.FormatUtils

class CurrencyDetailFragment : BaseFragment(), PurchaseDialog.PurchaseListener, SellCurrencyDialog.SellListener, TradeCurrencyDialog.TradeListener {

    private val viewModel: CurrencyDetailViewModel by viewModels { viewModelFactory }

    private val args: CurrencyDetailFragmentArgs by navArgs()

    private lateinit var binding: FragmentCurrencyDetailBinding
    private lateinit var currencyId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCurrencyDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        currencyId = args.currencyId
        setupObservables()
        viewModel.getCurrency(currencyId)
    }

    private fun renderView(item: CurrencyItem) {
        with(binding) {
            toolbar.title = item.name
            toolbar.navigationIcon =
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_arrow_left_black_24dp)
            toolbar.setNavigationOnClickListener {
                findNavController().navigateUp()
            }

            textViewPriceValue.text = FormatUtils.formatCurrency(item.price)
            textViewBalanceValue.text = FormatUtils.formatCurrency(item.balance)
            textViewBalanceUnit.text =
                getString(R.string.label_balance_unit, FormatUtils.formatUnit(item.balanceUnit))

            buttonPurchase.setOnClickListener {
                viewModel.showPurchaseScreen()
            }

            buttonSell.setOnClickListener {
                viewModel.showSellCurrencyScreen()
            }

            buttonTrade.setOnClickListener {
                viewModel.showTradeCurrencyScreen()
            }
        }
    }

    private fun setupObservables() {
        viewModel.showPurchaseScreen.observe(viewLifecycleOwner, { result ->
            PurchaseDialog.show(childFragmentManager, result.first, result.second)
        })

        viewModel.showSellCurrencyScreen.observe(viewLifecycleOwner, { result ->
            SellCurrencyDialog.show(childFragmentManager, result.first, result.second)
        })

        viewModel.showTradeCurrencyScreen.observe(viewLifecycleOwner, { tradeItem ->
            TradeCurrencyDialog.show(childFragmentManager, tradeItem)
        })

        viewModel.showSuccessScreen.observe(viewLifecycleOwner, {
            viewModel.getBalance()
            viewModel.getCurrency(currencyId)
            InfoDialog.show(childFragmentManager, R.drawable.ic_check_circle_48dp, it)
        })

        viewModel.showFailedScreen.observe(viewLifecycleOwner, {
            InfoDialog.show(
                childFragmentManager,
                R.drawable.ic_failed_48dp,
                R.string.transaction_failed_info
            )
        })

        viewModel.currency.observe(viewLifecycleOwner, ::renderView)
    }

    override fun onPurchaseClicked(unit: Double) {
        viewModel.purchase(unit)
    }

    override fun onSellClicked(unit: Double) {
        viewModel.sell(unit)
    }

    override fun onTradeClicked(item: CurrencyItem, unitTrade: Double, unitTraded: Double) {
        viewModel.trade(item, unitTrade, unitTraded)
    }
}
