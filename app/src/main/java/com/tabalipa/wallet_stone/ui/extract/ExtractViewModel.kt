package com.tabalipa.wallet_stone.ui.extract

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tabalipa.wallet_stone.interactor.GetExtractInteractor
import com.tabalipa.wallet_stone.model.domain.Transaction
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class ExtractViewModel @Inject constructor(
    private val getExtractInteractor: GetExtractInteractor
) : ViewModel() {

    private val _items = MutableLiveData<List<Transaction>>()
    val items: LiveData<List<Transaction>> = _items

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    init {
        loadExtract()
    }

    fun loadExtract() {
        _loading.value = true
        viewModelScope.launch {
            try {
                _items.value = getExtractInteractor.getExtract()
            } catch (e: Exception) {
                Timber.e(e)
            } finally {
                _loading.value = false
            }
        }
    }
}
