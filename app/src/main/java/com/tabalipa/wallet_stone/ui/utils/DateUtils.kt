package com.tabalipa.wallet_stone.ui.utils

import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

fun locale() = Locale("pt", "BR")

fun getCurrentDate(): String {
    val currentDate = LocalDate.now()
    val formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy")
    return currentDate.format(formatter)
}

fun get2DayPastDate(): String {
    val newDate = LocalDate.now().minusDays(2)
    val formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy")
    return newDate.format(formatter)
}

fun formatDateMillis(millis: Long): String {
    val date = Instant.ofEpochMilli(millis).atZone(ZoneId.systemDefault()).toLocalDate()
    val formatter = DateTimeFormatter.ofPattern("dd 'de' MMMM").withLocale(locale())
    return date.format(formatter)
}

fun getCurrentTimeInMillis(): Long {
    return OffsetDateTime.now()
        .toInstant()
        .toEpochMilli()
}
