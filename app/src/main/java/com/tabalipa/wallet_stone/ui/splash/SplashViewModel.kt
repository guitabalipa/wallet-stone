package com.tabalipa.wallet_stone.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tabalipa.wallet_stone.interactor.VerifyUserLoggedInteractor
import kotlinx.coroutines.launch
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val verifyUserLoggedInteractor: VerifyUserLoggedInteractor
) : ViewModel() {

    private val _isUserLogged = MutableLiveData<Boolean>()
    val isUserLogged: LiveData<Boolean> = _isUserLogged

    init {
        verifyUserLogged()
    }

    private fun verifyUserLogged() {
        viewModelScope.launch {
            _isUserLogged.value = verifyUserLoggedInteractor.isUserLogged()
        }
    }
}
