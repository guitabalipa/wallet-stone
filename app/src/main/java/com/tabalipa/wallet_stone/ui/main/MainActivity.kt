package com.tabalipa.wallet_stone.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.view.isVisible
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.tabalipa.wallet_stone.R
import com.tabalipa.wallet_stone.databinding.ActivityMainBinding
import com.tabalipa.wallet_stone.ui.base.BaseActivity

class MainActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context) = Intent(context, MainActivity::class.java)
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupBottomNavigation()
    }

    private fun setupBottomNavigation() {
        val navController = Navigation.findNavController(
            this@MainActivity,
            R.id.nav_host_fragment
        )
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.fragment_home,
                R.id.fragment_extract,
                R.id.fragment_about -> setBottomNavigationVisibility(true)
                else -> setBottomNavigationVisibility(false)
            }
        }

        binding.bottomNavigation.apply {
            setupWithNavController(navController)
            setOnNavigationItemReselectedListener {}
        }
    }

    private fun setBottomNavigationVisibility(isNavVisible: Boolean) {
        with(binding) {
            when {
                isNavVisible == bottomNavigation.isVisible -> return
                isNavVisible -> showBottomBar()
                else -> hideBottomBar()
            }
        }
    }

    private fun showBottomBar() {
        with(binding) {
            bottomNavigation.apply {
                translationY = root.measuredHeight.toFloat()
                isVisible = true
                animate()
                    .translationY(1f)
                    .setDuration(350)
                    .setInterpolator(AccelerateDecelerateInterpolator())
                    .start()
            }
        }
    }

    private fun hideBottomBar() {
        with(binding) {
            bottomNavigation.apply {
                animate()
                    .translationY(root.measuredHeight.toFloat())
                    .setDuration(350)
                    .setInterpolator(AccelerateDecelerateInterpolator())
                    .withEndAction { isVisible = false }
                    .start()
            }
        }
    }
}
