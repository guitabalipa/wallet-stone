package com.tabalipa.wallet_stone.ui.home

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tabalipa.wallet_stone.R
import com.tabalipa.wallet_stone.databinding.ItemCurrencyBinding
import com.tabalipa.wallet_stone.model.domain.CurrencyItem
import com.tabalipa.wallet_stone.ui.utils.FormatUtils

class CurrencyAdapter(private val onItemClick: (CurrencyItem) -> Unit) :
    ListAdapter<CurrencyItem, CurrencyViewHolder>(DIFF_UTIL) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CurrencyViewHolder(
            inflater.inflate(R.layout.item_currency, parent, false),
            onItemClick
        )
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val DIFF_UTIL = object : DiffUtil.ItemCallback<CurrencyItem>() {
            override fun areItemsTheSame(oldItem: CurrencyItem, newItem: CurrencyItem): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: CurrencyItem, newItem: CurrencyItem): Boolean {
                return oldItem == newItem
            }
        }
    }
}

class CurrencyViewHolder(itemView: View, private val onItemClick: (CurrencyItem) -> Unit) :
    RecyclerView.ViewHolder(itemView) {

    private val binding = ItemCurrencyBinding.bind(itemView)
    private val context = itemView.context
    private val textColor = ContextCompat.getColor(context, R.color.black)

    fun bind(item: CurrencyItem) {
        with(binding) {
            textViewCurrencyName.text = item.name
            textViewCurrencyPrice.text = SpannableStringBuilder(context.getString(R.string.label_price)).append(" ")
                .append(textFormat(FormatUtils.formatCurrency(item.price)))
            textViewBalanceValue.text = SpannableStringBuilder(context.getString(R.string.label_balance)).append(" ")
                .append(textFormat(FormatUtils.formatCurrency(item.balance)))
            textViewBalanceUnit.text = context.getString(R.string.label_balance_unit, FormatUtils.formatUnit(item.balanceUnit))
        }

        itemView.setOnClickListener { onItemClick.invoke(item) }
    }

    private fun textFormat(text: String): SpannableStringBuilder {
        return SpannableStringBuilder(text).apply {
            setSpan(ForegroundColorSpan(textColor), 0, this.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            setSpan(StyleSpan(Typeface.BOLD), 0, this.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
}
