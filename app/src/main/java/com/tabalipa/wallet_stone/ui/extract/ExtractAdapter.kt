package com.tabalipa.wallet_stone.ui.extract

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tabalipa.wallet_stone.R
import com.tabalipa.wallet_stone.databinding.ItemExtractBinding
import com.tabalipa.wallet_stone.helper.BalanceHelper
import com.tabalipa.wallet_stone.model.domain.Transaction
import com.tabalipa.wallet_stone.ui.currencydetail.TransactionType
import com.tabalipa.wallet_stone.ui.utils.FormatUtils
import com.tabalipa.wallet_stone.ui.utils.formatDateMillis

class ExtractAdapter(private val helper: BalanceHelper) : ListAdapter<Transaction, ExtractViewHolder>(DIFF_UTIL) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExtractViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ExtractViewHolder(inflater.inflate(R.layout.item_extract, parent, false), helper)
    }

    override fun onBindViewHolder(holder: ExtractViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val DIFF_UTIL = object : DiffUtil.ItemCallback<Transaction>() {
            override fun areItemsTheSame(oldItem: Transaction, newItem: Transaction): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Transaction, newItem: Transaction): Boolean {
                return oldItem == newItem
            }
        }
    }
}

class ExtractViewHolder(itemView: View, private val helper: BalanceHelper) : RecyclerView.ViewHolder(itemView) {

    private val binding = ItemExtractBinding.bind(itemView)
    private val context = itemView.context
    private val purchaseColor = ContextCompat.getColor(context, R.color.teal_200)
    private val sellColor = ContextCompat.getColor(context, R.color.red)
    private val tradeColor = ContextCompat.getColor(context, R.color.orange)

    fun bind(item: Transaction) {
        when (TransactionType.valueOf(item.type)) {
            TransactionType.PURCHASE -> setupPurchaseItem(item)
            TransactionType.SELL -> setupSellItem(item)
            TransactionType.TRADE -> setupTradeItem(item)
        }
    }

    private fun setupPurchaseItem(item: Transaction) {
        with(binding) {
            textViewTitle.text = textFormat(context.getString(R.string.label_purchase_currency, item.idCurrency), purchaseColor)
            textViewDate.text = formatDateMillis(item.date)
            textViewValue.text = context.getString(R.string.label_value, FormatUtils.formatCurrency(item.amountCurrency))
            textViewUnit.text = context.getString(R.string.label_purchased, FormatUtils.formatUnit(item.amountUnit))
            textViewPrice.text = context.getString(R.string.label_price_value, FormatUtils.formatCurrency(item.priceCurrency))
        }
    }

    private fun setupSellItem(item: Transaction) {
        with(binding) {
            textViewTitle.text = textFormat(context.getString(R.string.label_sell_currency, item.idCurrency), sellColor)
            textViewDate.text = formatDateMillis(item.date)
            textViewValue.text = context.getString(R.string.label_value, FormatUtils.formatCurrency(item.amountCurrency))
            textViewUnit.text = context.getString(R.string.label_sold, FormatUtils.formatUnit(item.amountUnit))
            textViewPrice.text = context.getString(R.string.label_price_value, FormatUtils.formatCurrency(item.priceCurrency))
        }
    }

    private fun setupTradeItem(item: Transaction) {
        with(binding) {
            textViewTitle.text = textFormat(context.getString(R.string.label_trade_currency, item.idCurrency, item.idCurrencyTraded), tradeColor)
            textViewDate.text = formatDateMillis(item.date)
            val currencyValue = helper.calculateBalance(item.priceCurrency, item.amountUnit)
            textViewValue.text = context.getString(R.string.label_value_sent, FormatUtils.formatCurrency(currencyValue))
            textViewUnit.text = context.getString(R.string.label_unit_received, FormatUtils.formatUnit(item.amountUnitTraded!!))
            textViewPrice.text = context.getString(R.string.label_price_value, FormatUtils.formatCurrency(item.priceCurrencyTraded!!))
        }
    }

    private fun textFormat(text: String, color: Int): SpannableStringBuilder {
        return SpannableStringBuilder(text).apply {
            setSpan(ForegroundColorSpan(color), 0, this.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
}
