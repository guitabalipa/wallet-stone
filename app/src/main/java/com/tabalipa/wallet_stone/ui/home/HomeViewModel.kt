package com.tabalipa.wallet_stone.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tabalipa.wallet_stone.interactor.GetAllCurrenciesInteractor
import com.tabalipa.wallet_stone.interactor.GetBalanceInteractor
import com.tabalipa.wallet_stone.model.domain.CurrencyItem
import com.tabalipa.wallet_stone.ui.utils.get2DayPastDate
import com.tabalipa.wallet_stone.ui.utils.getCurrentDate
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val getAllCurrenciesInteractor: GetAllCurrenciesInteractor,
    private val getBalanceInteractor: GetBalanceInteractor
) : ViewModel() {

    private val initialDate = "'${get2DayPastDate()}'"
    private val currentDate = "'${getCurrentDate()}'"

    private val _loading = MutableLiveData<Boolean>().apply { value = false }
    val loading: LiveData<Boolean> = _loading

    private val _items = MutableLiveData<List<CurrencyItem>>()
    val items: LiveData<List<CurrencyItem>> = _items

    private val _balance = MutableLiveData<Double>()
    val balance: LiveData<Double> = _balance

    fun getBalance() {
        viewModelScope.launch {
            try {
                _balance.value = getBalanceInteractor.getBalanceAvailableAmount()
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun getCurrencies() {
        _loading.value = true
        viewModelScope.launch {
            try {
                _items.value = getAllCurrenciesInteractor.getAllCurrencies(initialDate, currentDate)
            } catch (e: Exception) {
                Timber.e(e)
            } finally {
                _loading.value = false
            }
        }
    }
}
