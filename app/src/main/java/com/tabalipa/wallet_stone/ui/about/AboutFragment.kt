package com.tabalipa.wallet_stone.ui.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tabalipa.wallet_stone.BuildConfig
import com.tabalipa.wallet_stone.R
import com.tabalipa.wallet_stone.databinding.FragmentAboutBinding
import com.tabalipa.wallet_stone.ui.base.BaseFragment

class AboutFragment : BaseFragment() {

    private lateinit var binding: FragmentAboutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAboutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        binding.textViewAppVersion.text = getString(R.string.label_app_version, BuildConfig.VERSION_NAME)
    }
}
