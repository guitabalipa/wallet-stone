package com.tabalipa.wallet_stone.helper

class BalanceHelper {

    fun calculateBalance(price: Double, amount: Double): Double {
        return price * amount
    }

    fun getUnitAmount(amount: Double, price: Double): Double {
        return amount / price
    }
}
