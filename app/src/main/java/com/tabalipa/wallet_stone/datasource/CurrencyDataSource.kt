package com.tabalipa.wallet_stone.datasource

import com.tabalipa.wallet_stone.api.RestApi
import com.tabalipa.wallet_stone.model.remote.CurrencyRemote
import javax.inject.Inject

class CurrencyDataSource @Inject constructor(private val api: RestApi) {

    suspend fun getAllCurrencies(initialDate: String, endDate: String): List<CurrencyRemote> {
        val btcResponse = api.getBitcoinInfo()
        val btrResponse = api.getBritaInfo(initialDate, endDate)

        return listOf(
            CurrencyRemote(
                name = "Bitcoin",
                price = btcResponse.ticker.last.toDouble()
            ),
            CurrencyRemote(
                name = "Brita",
                price = btrResponse.value.last().cotacaoCompra
            )
        )
    }
}
