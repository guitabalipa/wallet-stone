package com.tabalipa.wallet_stone.datasource.db.dao

import androidx.room.*
import com.tabalipa.wallet_stone.model.entity.BalanceEntity

@Dao
interface BalanceDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insert(balance: BalanceEntity)

    @Update
    suspend fun update(balance: BalanceEntity)

    @Query("SELECT * FROM balance")
    suspend fun getAll(): List<BalanceEntity>

    @Query("SELECT * FROM balance WHERE id_user == :idUser AND id_currency == :idCurrency")
    suspend fun getBalanceByCurrency(idUser: String, idCurrency: String): BalanceEntity?
}
