package com.tabalipa.wallet_stone.datasource

import android.content.SharedPreferences
import androidx.core.content.edit
import javax.inject.Inject

class UserInfoLocalDataSource @Inject constructor(
    private val preferences: SharedPreferences
) {

    fun logUser() {
        preferences.edit(commit = true) {
            putBoolean(PREF_IS_USER_LOGGED, true)
        }
    }

    fun isUserLogged(): Boolean {
        return preferences.getBoolean(PREF_IS_USER_LOGGED, false)
    }

    fun setUsername(username: String) {
        preferences.edit(commit = true) {
            putString(PREF_LOGGED_USERNAME, username)
        }
    }

    fun getUsername(): String {
        return preferences.getString(PREF_LOGGED_USERNAME, "")!!
    }

    fun logout() {
        preferences.edit().clear().apply()
    }

    companion object {
        private const val PREF_IS_USER_LOGGED = "pref_is_user_logged"
        private const val PREF_LOGGED_USERNAME = "pref_logged_username"
    }
}
