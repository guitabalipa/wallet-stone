package com.tabalipa.wallet_stone.datasource.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tabalipa.wallet_stone.model.entity.CurrencyEntity

@Dao
interface CurrencyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(currencies: List<CurrencyEntity>)

    @Query("SELECT * FROM currency")
    suspend fun getAll(): List<CurrencyEntity>

    @Query("SELECT * FROM currency WHERE name == :currencyId")
    suspend fun getCurrencyById(currencyId: String): CurrencyEntity
}
