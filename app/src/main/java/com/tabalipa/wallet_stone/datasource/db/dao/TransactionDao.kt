package com.tabalipa.wallet_stone.datasource.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tabalipa.wallet_stone.model.entity.TransactionEntity

@Dao
interface TransactionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(transaction: TransactionEntity)

    @Query("SELECT * FROM currency_transaction WHERE idUser == :idUser ORDER BY date DESC")
    suspend fun getAllByUser(idUser: String): List<TransactionEntity>
}
