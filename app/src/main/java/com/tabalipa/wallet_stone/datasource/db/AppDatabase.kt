package com.tabalipa.wallet_stone.datasource.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.tabalipa.wallet_stone.datasource.db.dao.BalanceDao
import com.tabalipa.wallet_stone.datasource.db.dao.CurrencyDao
import com.tabalipa.wallet_stone.datasource.db.dao.TransactionDao
import com.tabalipa.wallet_stone.datasource.db.dao.UserDao
import com.tabalipa.wallet_stone.model.entity.BalanceEntity
import com.tabalipa.wallet_stone.model.entity.CurrencyEntity
import com.tabalipa.wallet_stone.model.entity.TransactionEntity
import com.tabalipa.wallet_stone.model.entity.UserEntity

@Database(
    entities = [CurrencyEntity::class, UserEntity::class, BalanceEntity::class, TransactionEntity::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun currencyDao(): CurrencyDao
    abstract fun userDao(): UserDao
    abstract fun balanceDao(): BalanceDao
    abstract fun transactionDao(): TransactionDao
}
