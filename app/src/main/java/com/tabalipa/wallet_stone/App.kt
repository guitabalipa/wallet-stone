package com.tabalipa.wallet_stone

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.tabalipa.wallet_stone.di.component.ApplicationComponent
import com.tabalipa.wallet_stone.di.component.DaggerApplicationComponent
import timber.log.Timber

class App : Application() {

    val appComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .application(this)
            .build()
    }

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)
        Timber.plant(Timber.DebugTree())
    }
}
