package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.Constants
import com.tabalipa.wallet_stone.R
import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import com.tabalipa.wallet_stone.datasource.db.dao.TransactionDao
import com.tabalipa.wallet_stone.model.domain.Transaction
import com.tabalipa.wallet_stone.model.mapper.toEntity
import timber.log.Timber
import javax.inject.Inject

class SavePurchaseTransactionInteractor @Inject constructor(
    private val transactionDao: TransactionDao,
    private val userInfoLocalDataSource: UserInfoLocalDataSource,
    private val updateBalanceInteractor: UpdateBalanceInteractor
) {

    suspend fun save(transaction: Transaction, balanceUnit: Double, balance: Double): Result {
        return try {
            transactionDao.insert(transaction.toEntity(userInfoLocalDataSource.getUsername()))
            updateBalanceInteractor.update(transaction.idCurrency, transaction.amountUnit + balanceUnit)
            updateBalanceInteractor.update(Constants.DEFAULT_CURRENCY, balance - transaction.amountCurrency)
            Result.Success(R.string.purchase_success_info)
        } catch (e: Exception) {
            Timber.e(e)
            Result.Failure
        }
    }

    sealed class Result {
        data class Success(val text: Int) : Result()
        object Failure : Result()
    }
}
