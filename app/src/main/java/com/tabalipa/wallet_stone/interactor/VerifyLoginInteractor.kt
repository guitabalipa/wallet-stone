package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.db.dao.UserDao
import com.tabalipa.wallet_stone.model.domain.User
import timber.log.Timber
import javax.inject.Inject

class VerifyLoginInteractor @Inject constructor(
    private val userDao: UserDao,
    private val logUserInteractor: LogUserInteractor,
    private val setUsernameInteractor: SetUsernameInteractor
) {

    suspend fun verify(user: User): Result {
        return try {
            val userEntity = userDao.getUser(user.username)

            if (userEntity.password == user.password) {
                logUserInteractor.logUser()
                setUsernameInteractor.setUsername(user.username)
                Result.Success
            } else {
                Result.Failure
            }
        } catch (e: Exception) {
            Timber.e(e)
            Result.Failure
        }
    }

    sealed class Result {
        object Success : Result()
        object Failure : Result()
    }
}
