package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import com.tabalipa.wallet_stone.datasource.db.dao.BalanceDao
import com.tabalipa.wallet_stone.helper.BalanceHelper
import com.tabalipa.wallet_stone.model.domain.CurrencyItem
import com.tabalipa.wallet_stone.model.mapper.toDomain
import com.tabalipa.wallet_stone.repository.CurrencyRepository
import javax.inject.Inject

class GetCurrencyInteractor @Inject constructor(
    private val repository: CurrencyRepository,
    private val balanceDao: BalanceDao,
    private val balanceHelper: BalanceHelper,
    private val infoLocalDataSource: UserInfoLocalDataSource
) {

    suspend fun getCurrency(currencyId: String): CurrencyItem {
        val balanceEntity =
            balanceDao.getBalanceByCurrency(infoLocalDataSource.getUsername(), currencyId)
        val currency = repository.getCurrency(currencyId)
        return currency.toDomain(balanceEntity?.amount, balanceHelper)
    }
}
