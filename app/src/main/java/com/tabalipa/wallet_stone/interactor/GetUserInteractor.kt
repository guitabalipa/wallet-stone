package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.db.dao.UserDao
import com.tabalipa.wallet_stone.model.domain.User
import com.tabalipa.wallet_stone.model.mapper.toDomain
import javax.inject.Inject

class GetUserInteractor @Inject constructor(private val userDao: UserDao) {

    suspend fun getUser(username: String): User {
        return userDao.getUser(username).toDomain()
    }
}
