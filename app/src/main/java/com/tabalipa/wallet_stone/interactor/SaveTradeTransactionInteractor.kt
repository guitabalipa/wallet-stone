package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.R
import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import com.tabalipa.wallet_stone.datasource.db.dao.BalanceDao
import com.tabalipa.wallet_stone.datasource.db.dao.TransactionDao
import com.tabalipa.wallet_stone.model.domain.TradeTransaction
import com.tabalipa.wallet_stone.model.mapper.toEntity
import timber.log.Timber
import javax.inject.Inject

class SaveTradeTransactionInteractor @Inject constructor(
    private val transactionDao: TransactionDao,
    private val balanceDao: BalanceDao,
    private val userInfoLocalDataSource: UserInfoLocalDataSource,
    private val updateBalanceInteractor: UpdateBalanceInteractor
) {

    suspend fun save(transaction: TradeTransaction, balanceUnit: Double): Result {
        return try {
            val userId = userInfoLocalDataSource.getUsername()
            val tradedBalance = balanceDao.getBalanceByCurrency(userId, transaction.idCurrencyTraded)
            transactionDao.insert(transaction.toEntity(userId))
            updateBalanceInteractor.update(
                transaction.idCurrency,
                balanceUnit - transaction.amountUnit
            )
            updateBalanceInteractor.update(
                transaction.idCurrencyTraded,
                tradedBalance?.run { amount + transaction.amountUnitTraded } ?: transaction.amountUnitTraded
            )
            Result.Success(R.string.sell_success_info)
        } catch (e: Exception) {
            Timber.e(e)
            Result.Failure
        }
    }

    sealed class Result {
        data class Success(val text: Int) : Result()
        object Failure : Result()
    }
}
