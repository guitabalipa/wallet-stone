package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.db.dao.BalanceDao
import com.tabalipa.wallet_stone.model.domain.Balance
import com.tabalipa.wallet_stone.model.mapper.toEntity
import javax.inject.Inject

class SaveBalanceInteractor @Inject constructor(private val balanceDao: BalanceDao) {

    suspend fun save(balance: Balance) {
        balanceDao.insert(balance.toEntity())
    }
}
