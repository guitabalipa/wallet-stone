package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.Constants
import com.tabalipa.wallet_stone.datasource.db.dao.CurrencyDao
import com.tabalipa.wallet_stone.model.domain.CurrencyItem
import com.tabalipa.wallet_stone.model.mapper.toDomain
import javax.inject.Inject

class GetTradeCurrenciesInteractor @Inject constructor(
    private val currencyDao: CurrencyDao
) {

    suspend fun getCurrencies(currentCurrencyId: String): List<CurrencyItem> {
        return currencyDao.getAll()
            .filter { it.name != currentCurrencyId && it.name != Constants.DEFAULT_CURRENCY }
            .map { it.toDomain() }
    }
}
