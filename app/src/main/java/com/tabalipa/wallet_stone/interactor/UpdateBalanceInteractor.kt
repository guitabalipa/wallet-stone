package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import com.tabalipa.wallet_stone.datasource.db.dao.BalanceDao
import com.tabalipa.wallet_stone.model.domain.Balance
import javax.inject.Inject

class UpdateBalanceInteractor @Inject constructor(
    private val balanceDao: BalanceDao,
    private val saveBalanceInteractor: SaveBalanceInteractor,
    private val userInfoLocalDataSource: UserInfoLocalDataSource
) {

    suspend fun update(currencyId: String, amount: Double) {
        val username = userInfoLocalDataSource.getUsername()
        val balanceEntity = balanceDao.getBalanceByCurrency(username, currencyId)

        if (balanceEntity != null) {
            balanceDao.update(balanceEntity.copy(amount = amount))
        } else {
            saveBalanceInteractor.save(Balance(username, currencyId, amount))
        }
    }
}
