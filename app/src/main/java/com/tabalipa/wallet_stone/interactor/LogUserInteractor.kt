package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LogUserInteractor @Inject constructor(
    private val userInfoLocalDataSource: UserInfoLocalDataSource
) {

    suspend fun logUser() = withContext(Dispatchers.Default) {
        userInfoLocalDataSource.logUser()
    }
}
