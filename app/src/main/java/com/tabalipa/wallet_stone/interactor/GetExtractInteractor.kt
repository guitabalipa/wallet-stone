package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import com.tabalipa.wallet_stone.datasource.db.dao.TransactionDao
import com.tabalipa.wallet_stone.model.domain.Transaction
import com.tabalipa.wallet_stone.model.mapper.toDomain
import javax.inject.Inject

class GetExtractInteractor @Inject constructor(
    private val transactionDao: TransactionDao,
    private val userInfoLocalDataSource: UserInfoLocalDataSource
) {

    suspend fun getExtract(): List<Transaction> {
        val username = userInfoLocalDataSource.getUsername()
        return transactionDao.getAllByUser(username).map { it.toDomain() }
    }
}
