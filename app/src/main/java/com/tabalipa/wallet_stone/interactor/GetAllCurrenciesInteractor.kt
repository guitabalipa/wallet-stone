package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import com.tabalipa.wallet_stone.datasource.db.dao.BalanceDao
import com.tabalipa.wallet_stone.helper.BalanceHelper
import com.tabalipa.wallet_stone.model.domain.CurrencyItem
import com.tabalipa.wallet_stone.model.entity.BalanceEntity
import com.tabalipa.wallet_stone.model.entity.CurrencyEntity
import com.tabalipa.wallet_stone.model.mapper.toDomain
import com.tabalipa.wallet_stone.repository.CurrencyRepository
import javax.inject.Inject

class GetAllCurrenciesInteractor @Inject constructor(
    private val repository: CurrencyRepository,
    private val balanceDao: BalanceDao,
    private val balanceHelper: BalanceHelper,
    private val userInfoLocalDataSource: UserInfoLocalDataSource
) {

    suspend fun getAllCurrencies(initialDate: String, endDate: String): List<CurrencyItem> {
        return mapCurrenciesToItem(
            repository.getAllCurrencies(initialDate, endDate),
            balanceDao.getAll()
        )
    }

    private fun mapCurrenciesToItem(
        allCurrencies: List<CurrencyEntity>,
        allBalance: List<BalanceEntity>
    ): List<CurrencyItem> {
        val username = userInfoLocalDataSource.getUsername()
        return allCurrencies.map { currencyEntity ->
            val balanceEntity =
                allBalance.find { entity -> entity.idCurrency == currencyEntity.name && entity.idUser == username }
            currencyEntity.toDomain(balanceEntity?.amount, balanceHelper)
        }
    }
}
