package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.db.dao.UserDao
import com.tabalipa.wallet_stone.model.domain.Balance
import com.tabalipa.wallet_stone.model.domain.User
import com.tabalipa.wallet_stone.model.mapper.toEntity
import timber.log.Timber
import javax.inject.Inject

class SaveUserInteractor @Inject constructor(
    private val userDao: UserDao,
    private val setUsernameInteractor: SetUsernameInteractor,
    private val saveBalanceInteractor: SaveBalanceInteractor,
    private val logUserInteractor: LogUserInteractor
) {

    suspend fun save(user: User, balance: Balance): Result {
        return try {
            userDao.insert(user.toEntity())
            setUsernameInteractor.setUsername(user.username)
            saveBalanceInteractor.save(balance)
            logUserInteractor.logUser()
            Result.Success
        } catch (e: Exception) {
            Timber.e(e)
            Result.Failure
        }
    }

    sealed class Result {
        object Success : Result()
        object Failure : Result()
    }
}
