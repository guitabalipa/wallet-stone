package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.Constants
import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import com.tabalipa.wallet_stone.datasource.db.dao.BalanceDao
import javax.inject.Inject

class GetBalanceInteractor @Inject constructor(
    private val balanceDao: BalanceDao,
    private val userInfoLocalDataSource: UserInfoLocalDataSource
) {

    suspend fun getBalanceAvailableAmount(): Double {
        val balance = balanceDao.getBalanceByCurrency(userInfoLocalDataSource.getUsername(), Constants.DEFAULT_CURRENCY)
        return balance!!.amount
    }
}
