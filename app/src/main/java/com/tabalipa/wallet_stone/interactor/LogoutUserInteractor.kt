package com.tabalipa.wallet_stone.interactor

import com.tabalipa.wallet_stone.datasource.UserInfoLocalDataSource
import javax.inject.Inject

class LogoutUserInteractor @Inject constructor(
    private val userInfoLocalDataSource: UserInfoLocalDataSource
) {

    fun logout() {
        userInfoLocalDataSource.logout()
    }
}
