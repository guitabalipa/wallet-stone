package com.tabalipa.wallet_stone.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "balance", primaryKeys = ["id_user", "id_currency"])
data class BalanceEntity(
    @ColumnInfo(name = "id_user")
    val idUser: String,
    @ColumnInfo(name = "id_currency")
    val idCurrency: String,
    val amount: Double
)
