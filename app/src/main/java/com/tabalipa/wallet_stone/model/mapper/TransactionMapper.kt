package com.tabalipa.wallet_stone.model.mapper

import com.tabalipa.wallet_stone.model.domain.TradeTransaction
import com.tabalipa.wallet_stone.model.domain.Transaction
import com.tabalipa.wallet_stone.model.entity.TransactionEntity
import java.util.*

fun Transaction.toEntity(idUser: String): TransactionEntity {
    return TransactionEntity(
        id = UUID.randomUUID().toString(),
        idUser = idUser,
        idCurrency = idCurrency,
        type = type,
        date = date,
        priceCurrency = priceCurrency,
        amountCurrency = amountCurrency,
        amountUnit = amountUnit
    )
}

fun TransactionEntity.toDomain(): Transaction {
    return Transaction(
        idCurrency = idCurrency,
        type = type,
        date = date,
        priceCurrency = priceCurrency,
        amountCurrency = amountCurrency ?: .0,
        amountUnit = amountUnit,
        idCurrencyTraded = idCurrencyTraded,
        priceCurrencyTraded = priceCurrencyTraded,
        amountUnitTraded = amountUnitTraded
    )
}

fun TradeTransaction.toEntity(idUser: String): TransactionEntity {
    return TransactionEntity(
        id = UUID.randomUUID().toString(),
        idUser = idUser,
        idCurrency = idCurrency,
        idCurrencyTraded = idCurrencyTraded,
        type = type,
        date = date,
        priceCurrency = priceCurrency,
        priceCurrencyTraded = priceCurrencyTraded,
        amountUnit = amountUnit,
        amountUnitTraded = amountUnitTraded
    )
}
