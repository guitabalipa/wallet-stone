package com.tabalipa.wallet_stone.model.remote

data class CurrencyRemote(
    val name: String,
    val price: Double
)
