package com.tabalipa.wallet_stone.model.domain

data class User(
    val username: String,
    val password: String
)
