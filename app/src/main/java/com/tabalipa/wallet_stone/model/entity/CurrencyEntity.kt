package com.tabalipa.wallet_stone.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currency")
data class CurrencyEntity(
    @PrimaryKey
    val name: String,
    val price: Double
)
