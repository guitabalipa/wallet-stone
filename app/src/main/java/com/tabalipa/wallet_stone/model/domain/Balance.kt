package com.tabalipa.wallet_stone.model.domain

data class Balance(
    val idUser: String,
    val idCurrency: String,
    val amount: Double
)
