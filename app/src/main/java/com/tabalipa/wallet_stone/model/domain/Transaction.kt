package com.tabalipa.wallet_stone.model.domain

data class Transaction(
    val idCurrency: String,
    val type: String,
    val date: Long,
    val priceCurrency: Double,
    val amountCurrency: Double,
    val amountUnit: Double,
    val idCurrencyTraded: String? = "",
    val priceCurrencyTraded: Double? = .0,
    val amountUnitTraded: Double? = .0
)
