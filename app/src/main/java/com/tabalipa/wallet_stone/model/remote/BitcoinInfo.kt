package com.tabalipa.wallet_stone.model.remote

data class BitcoinInfo(
    val high: String,
    val low: String,
    val vol: String,
    val last: String,
    val buy: String,
    val sell: String,
    val open: String,
)
