package com.tabalipa.wallet_stone.model.remote

data class BritaInfo(
    val cotacaoCompra: Double,
    val cotacaoVenda: Double,
    val dataHoraCotacao: String
)
