package com.tabalipa.wallet_stone.model.remote

import com.squareup.moshi.Json

data class BritaResponse(
    @Json(name = "@odata.context")
    val data: String,
    val value: List<BritaInfo>
)
