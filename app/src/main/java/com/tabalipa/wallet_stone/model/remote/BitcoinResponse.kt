package com.tabalipa.wallet_stone.model.remote

data class BitcoinResponse(
    val ticker: BitcoinInfo
)
