package com.tabalipa.wallet_stone.model.mapper

import com.tabalipa.wallet_stone.helper.BalanceHelper
import com.tabalipa.wallet_stone.model.domain.CurrencyItem
import com.tabalipa.wallet_stone.model.entity.CurrencyEntity
import com.tabalipa.wallet_stone.model.remote.CurrencyRemote

fun CurrencyRemote.toEntity(): CurrencyEntity {
    return CurrencyEntity(
        name = name,
        price = price
    )
}

fun CurrencyEntity.toDomain(): CurrencyItem {
    return CurrencyItem(
        name = name,
        price = price,
        balance = .0,
        balanceUnit = .0
    )
}

fun CurrencyEntity.toDomain(balanceUnit: Double?, balanceHelper: BalanceHelper): CurrencyItem {
    val balance = if (balanceUnit != null) {
        balanceHelper.calculateBalance(price, balanceUnit)
    } else .0
    return CurrencyItem(
        name = name,
        price = price,
        balance = balance,
        balanceUnit = balanceUnit ?: .0
    )
}
