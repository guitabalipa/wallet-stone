package com.tabalipa.wallet_stone.model.mapper

import com.tabalipa.wallet_stone.model.domain.User
import com.tabalipa.wallet_stone.model.entity.UserEntity

fun User.toEntity(): UserEntity {
    return UserEntity(
        username = username,
        password = password
    )
}

fun UserEntity.toDomain(): User {
    return User(
        username = username,
        password = password
    )
}
