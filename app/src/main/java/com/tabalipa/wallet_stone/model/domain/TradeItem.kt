package com.tabalipa.wallet_stone.model.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TradeItem(
    val balance: Double,
    val price: Double,
    val currencies: List<CurrencyItem>
) : Parcelable
