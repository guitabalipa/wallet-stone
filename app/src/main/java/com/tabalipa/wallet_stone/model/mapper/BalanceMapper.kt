package com.tabalipa.wallet_stone.model.mapper

import com.tabalipa.wallet_stone.model.domain.Balance
import com.tabalipa.wallet_stone.model.entity.BalanceEntity

fun Balance.toEntity(): BalanceEntity {
    return BalanceEntity(
        idUser = idUser,
        idCurrency = idCurrency,
        amount = amount
    )
}

fun BalanceEntity.toDomain(): Balance {
    return Balance(
        idUser = idUser,
        idCurrency = idCurrency,
        amount = amount
    )
}
