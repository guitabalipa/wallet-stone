package com.tabalipa.wallet_stone.model.domain

data class TradeTransaction(
    val idCurrency: String,
    val idCurrencyTraded: String,
    val type: String,
    val date: Long,
    val priceCurrency: Double,
    val priceCurrencyTraded: Double,
    val amountUnit: Double,
    val amountUnitTraded: Double
)
