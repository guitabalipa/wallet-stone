package com.tabalipa.wallet_stone.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class UserEntity(
    @PrimaryKey
    val username: String,
    val password: String
)
