package com.tabalipa.wallet_stone.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currency_transaction")
data class TransactionEntity(
    @PrimaryKey
    val id: String,
    val idUser: String,
    val idCurrency: String,
    val type: String,
    val date: Long,
    val priceCurrency: Double,
    val amountCurrency: Double? = .0,
    val amountUnit: Double,
    val idCurrencyTraded: String? = "",
    val priceCurrencyTraded: Double? = .0,
    val amountUnitTraded: Double? = .0
)
