package com.tabalipa.wallet_stone.model.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CurrencyItem(
    val name: String,
    val price: Double,
    val balance: Double,
    val balanceUnit: Double
): Parcelable
