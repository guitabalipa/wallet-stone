package com.tabalipa.wallet_stone.di.module

import androidx.lifecycle.ViewModel
import com.tabalipa.wallet_stone.di.ViewModelKey
import com.tabalipa.wallet_stone.ui.about.AboutViewModel
import com.tabalipa.wallet_stone.ui.currencydetail.CurrencyDetailViewModel
import com.tabalipa.wallet_stone.ui.extract.ExtractViewModel
import com.tabalipa.wallet_stone.ui.home.HomeViewModel
import com.tabalipa.wallet_stone.ui.login.LoginViewModel
import com.tabalipa.wallet_stone.ui.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExtractViewModel::class)
    abstract fun bindExtractViewModel(viewModel: ExtractViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AboutViewModel::class)
    abstract fun bindAboutViewModel(viewModel: AboutViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CurrencyDetailViewModel::class)
    abstract fun bindCurrencyDetailViewModel(viewModel: CurrencyDetailViewModel): ViewModel
}
