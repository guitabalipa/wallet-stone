package com.tabalipa.wallet_stone.di.scope

import javax.inject.Scope

@Scope
annotation class PresentationScope
