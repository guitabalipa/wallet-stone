package com.tabalipa.wallet_stone.di.module

import android.app.Application
import androidx.room.Room
import com.tabalipa.wallet_stone.datasource.db.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(application: Application): AppDatabase {
        val databaseBuilder = Room.databaseBuilder(application, AppDatabase::class.java, "app.db")
        return databaseBuilder
            .fallbackToDestructiveMigration()
            .build()
    }
}
