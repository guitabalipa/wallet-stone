package com.tabalipa.wallet_stone.di.component

import android.app.Application
import com.tabalipa.wallet_stone.di.module.DataModule
import com.tabalipa.wallet_stone.di.module.DatabaseModule
import com.tabalipa.wallet_stone.di.module.InterceptorsModule
import com.tabalipa.wallet_stone.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    NetworkModule::class,
    InterceptorsModule::class,
    DatabaseModule::class,
    DataModule::class
])
interface ApplicationComponent {
    fun activityComponentBuilder(): ActivityComponent.Builder

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): ApplicationComponent
    }
}
