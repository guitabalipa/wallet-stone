package com.tabalipa.wallet_stone.di.component

import com.tabalipa.wallet_stone.di.module.ViewModelModule
import com.tabalipa.wallet_stone.di.scope.PresentationScope
import com.tabalipa.wallet_stone.ui.splash.SplashActivity
import com.tabalipa.wallet_stone.ui.about.AboutFragment
import com.tabalipa.wallet_stone.ui.currencydetail.CurrencyDetailFragment
import com.tabalipa.wallet_stone.ui.currencydetail.PurchaseDialog
import com.tabalipa.wallet_stone.ui.currencydetail.SellCurrencyDialog
import com.tabalipa.wallet_stone.ui.currencydetail.TradeCurrencyDialog
import com.tabalipa.wallet_stone.ui.extract.ExtractFragment
import com.tabalipa.wallet_stone.ui.home.HomeFragment
import com.tabalipa.wallet_stone.ui.login.LoginActivity
import dagger.Subcomponent

@PresentationScope
@Subcomponent(modules = [ViewModelModule::class])
interface PresentationComponent {
    fun inject(fragment: HomeFragment)
    fun inject(fragment: ExtractFragment)
    fun inject(fragment: AboutFragment)
    fun inject(fragment: CurrencyDetailFragment)
    fun inject(activity: LoginActivity)
    fun inject(activity: SplashActivity)
    fun inject(dialog: PurchaseDialog)
    fun inject(dialog: SellCurrencyDialog)
    fun inject(dialog: TradeCurrencyDialog)
}
