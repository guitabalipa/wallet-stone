package com.tabalipa.wallet_stone.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.tabalipa.wallet_stone.datasource.db.AppDatabase
import com.tabalipa.wallet_stone.datasource.db.dao.BalanceDao
import com.tabalipa.wallet_stone.datasource.db.dao.CurrencyDao
import com.tabalipa.wallet_stone.datasource.db.dao.TransactionDao
import com.tabalipa.wallet_stone.datasource.db.dao.UserDao
import com.tabalipa.wallet_stone.helper.BalanceHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): SharedPreferences {
        return application.getSharedPreferences("app_prefs", Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun providesBalanceHelper(): BalanceHelper {
        return BalanceHelper()
    }

    @Provides
    @Singleton
    fun providesCorrectionDao(database: AppDatabase): CurrencyDao {
        return database.currencyDao()
    }

    @Provides
    @Singleton
    fun providesUserDao(database: AppDatabase): UserDao {
        return database.userDao()
    }

    @Provides
    @Singleton
    fun providesBalanceDao(database: AppDatabase): BalanceDao {
        return database.balanceDao()
    }

    @Provides
    @Singleton
    fun providesTransactionDao(database: AppDatabase): TransactionDao {
        return database.transactionDao()
    }
}
