package com.tabalipa.wallet_stone.api

import com.tabalipa.wallet_stone.model.remote.BitcoinResponse
import com.tabalipa.wallet_stone.model.remote.BritaResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface RestApi {

    @GET("https://www.mercadobitcoin.net/api/BTC/ticker")
    suspend fun getBitcoinInfo(): BitcoinResponse

    @GET("https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoDolarPeriodo(dataInicial=@dataInicial,dataFinalCotacao=@dataFinalCotacao)")
    suspend fun getBritaInfo(@Query("@dataInicial") initialDate: String, @Query("@dataFinalCotacao") endDate: String): BritaResponse
}
